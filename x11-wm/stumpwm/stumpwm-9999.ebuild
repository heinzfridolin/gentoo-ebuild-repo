# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 common-lisp-3 autotools desktop xdg-utils

DESCRIPTION="Stumpwm is a Window Manager written entirely in Common Lisp."
HOMEPAGE="https://stumpwm.github.io/"
EGIT_REPO_URI="https://github.com/${PN}/${PN}"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="contrib doc"

RESTRICT="strip mirror"

RDEPEND="dev-lisp/alexandria
		dev-lisp/cl-ppcre
		dev-lisp/clx
		>=dev-lisp/sbcl-1.3.0"
DEPEND="${RDEPEND}
		sys-apps/texinfo
		doc? ( virtual/texi2dvi )"

PDEPEND="contrib? ( x11-wm/stumpwm-contrib )"

CLPKGDIR="${CLSOURCEROOT}/${CLPACKAGE}"

install_docs() {
	local pdffile="${PN}.pdf"

	texi2pdf -o "${pdffile}" "${PN}.texi.in" && dodoc "${pdffile}" || die
	dodoc AUTHORS NEWS README.md
	doinfo "${PN}.info"
	docinto examples
	dodoc sample-stumpwmrc.lisp
}

src_prepare() {
	default
	eautoreconf
}

src_configure() {
	xdg_environment_reset
	econf
}

src_compile() {
	emake -j1
}

src_install() {
	emake destdir="${D}" install

	common-lisp-install-sources *.lisp
	common-lisp-install-asdf
	# Fix ASDF dir
	sed -i -e "/(:directory/c\   (:directory \"${CLPKGDIR}\")" \
		"${D}${CLPKGDIR}/load-stumpwm.lisp" || die
	use doc && install_docs
}
