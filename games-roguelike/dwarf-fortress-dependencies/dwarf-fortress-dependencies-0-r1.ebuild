# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A single-player fantasy game"
HOMEPAGE="https://www.bay12games.com/dwarves"

SLOT="0"
KEYWORDS="amd64 x86"
IUSE="debug"
RESTRICT="strip"

RDEPEND="media-libs/glew:0
	media-libs/libsdl[joystick,video]
	media-libs/sdl-image[png]
	media-libs/sdl-ttf
	sys-libs/zlib
	virtual/glu
	x11-libs/gtk+:2"
# Yup, libsndfile, openal and ncurses are only needed at compile-time; the code
# dlopens them at runtime if requested.
DEPEND="${RDEPEND}
	media-libs/libsndfile
	media-libs/openal
	sys-libs/ncurses-compat:5[unicode]
	virtual/pkgconfig"
