# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Simple rougelike game"
HOMEPAGE="https://github.com/tmewett/BrogueCE"
SRC_URI="https://github.com/tmewett/${PN}/archive/v${PV}.tar.gz"

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

RDEPEND="media-libs/libsdl2[video]
	media-libs/sdl2-image[png]"
DEPEND="${RDEPEND}"
BDEPEND=""

src_compile() {
	emake DATADIR="/opt/brogue"
}
src_install() {
	dodir /opt/brogue
	dodir /opt/brogue/assets
	exeinto /opt/brogue
	doexe bin/brogue
	doexe linux/brogue-multiuser.sh
	doexe bin/keymap.txt
	exeinto /opt/brogue/assets
	doexe bin/assets/*
	dosym /opt/brogue/brogue-multiuser.sh /usr/bin/brogue

	dodoc README.md CHANGELOG.md BUILD.md
}
